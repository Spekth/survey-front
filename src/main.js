import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
/** instalataciones personalizadas */
import axios from 'axios'
import VueAxios from 'vue-axios'
import 'bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css'
import 'bootstrap-icons/font/bootstrap-icons.css'

Vue.use(VueAxios, axios)

Vue.config.productionTip = false

/** Default url API - NODEJS  */
axios.defaults.baseURL = 'http://localhost:3000/v1';

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')